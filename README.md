# AC Fuzzer #

A [Dharma](https://github.com/MozillaSecurity/dharma) grammar file to test AC/DC compilers.

This project is created to test a programming assignment for the Compiler Design course (Fall 2019) at National Taiwan University.

## Dependencies ##

- Dharma
- Coreutils
- Bash
- Gawk

## Usage ##

One may need to apply `dharma.patch` to `dharma.py` in your Dharma installation so that Dharma can generate variables with purely alphabetical names.

Place your compiler binary at `./AcDc`, then run `./test.sh n`, where `n` is the number of test cases that you want to generate.

The test cases are written into `gen/`, and the output of Dharma is written into `out/seed`. In addition, for each test case, `out/*/out` contains the output of your compiler, and `out/*/{stdout,stderr}` contains the standard output and error respectively.

Note that the AC output may contain calculations that overflow 32-bit integers.
