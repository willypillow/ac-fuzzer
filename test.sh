#!/bin/bash
mkdir -p out
dharma -grammars ac.dg -count $1 -storage gen 2>&1 | tee out/seed
for i in $(seq 1 $1); do
	gawk 'BEGIN { prnt = ""; } { print; } match($0, /^[if] (.*)/, a) { prnt = prnt "p " a[1] "\n" } END { print prnt; }' gen/$1.html > tmp
	mv tmp gen/$1.html
	mkdir -p out/$i
	./AcDc gen/$i.html out/$i/out >out/$i/stdout 2>out/$i/stderr || echo "[RE]: $i"
done
